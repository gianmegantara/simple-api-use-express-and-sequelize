const Sequelize = require('sequelize')
const connection = require('./connection')

const books = connection.define('books', {
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    title: {
        type: Sequelize.STRING
    },
    author: {
        type: Sequelize.STRING
    },
    published_date: {
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW
    },
    pages: {
        type: Sequelize.INTEGER
    },
    language: {
        type: Sequelize.STRING
    },
    publisher_id: {
        type: Sequelize.STRING
    }
})

module.exports = books