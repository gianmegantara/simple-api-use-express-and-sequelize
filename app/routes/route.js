const express = require('express')
const bookController = require('../controllers/books')
const userController = require('../controllers/users')
const {
    validationRules,
    validate
} = require('../middleware/inputValidation')
const router = express.Router()

router.post('/register', userController.signUpUser)
router.post('/books', validationRules(), validate, bookController.postBook)
router.get('/books', bookController.getBook)
router.get('/books/:id', bookController.getBookById)
router.put('/books/:id', bookController.putBook)
router.delete('/books/:id', bookController.deleteById)

module.exports = router