const books = require('../../models/books')

exports.postBook = (req, res) => {
    const {
        title,
        author,
        publish_date,
        pages,
        language,
        publisher_id
    } = req.body
    books.create({
            title: title,
            author: author,
            publish_date: publish_date,
            pages: pages,
            language: language,
            publisher_id: publisher_id
        }).then(result => res.status(201).json({
            error: false,
            data: result,
            message: 'data was created'
        }))
        .catch(err => {
            console.log(err)
        })
}

exports.getBook = (req, res) => {
    books.findAll()
        .then(result => {
            res.status(201).json({
                error: false,
                data: result,
                message: 'get data succes'
            })
        }).catch(err => {
            console.log(err)
        })
}

exports.getBookById = (req, res) => {
    const id_book = req.params.id
    books.findByPk(id_book)
        .then(result => {
            res.status(201).json({
                error: false,
                data: result,
                message: 'get data by Id success'
            })
        })
        .catch(err => {
            console.log(err)
        })
}

exports.putBook = (req, res) => {
    const {
        title,
        author,
        publish_date,
        pages,
        language,
        publisher_id
    } = req.body
    const id_book = req.params.id
    console.log(id_book)
    books.findByPk(id_book)
        .then(result => {
            console.log(result)
            if (!result) {
                res.status(500).send({
                    message: 'no id found for update'
                })
            } else {
                books.update({
                        title: title,
                        author: author,
                        publish_date: publish_date,
                        pages: pages,
                        language: language,
                        publisher_id: publisher_id
                    }, {
                        where: {
                            id: id_book
                        }
                    }).then(result => {
                        res.json({
                            message: 'book was updated'
                        })
                    })
                    .catch(err => {
                        console.log(err)
                    })
            }
        }).catch(err => {
            console.log(err)
        })

}

exports.deleteById = (req, res) => {
    const id_book = req.params.id
    books.findByPk(id_book)
        .then(result => {
            console.log(result)
            if (!result) {
                res.status(500).send({
                    message: 'no data found for delete'
                })
            } else {
                books.destroy({
                        where: {
                            id: id_book
                        }
                    })
                    .then(result => {
                        res.status(201).json({
                            error: false,
                            message: `data id : ${id_book} was succesfully deleted`
                        })
                    }).catch(err => {
                        console.log(err)
                    })
            }
        })
        .catch(err => {
            console.log(err)
        })

}