const users = require('../../models/users')
const bcrypt = require('bcryptjs')

exports.signUpUser = (req, res) => {
    const {
        name,
        username,
        email,
        password
    } = req.body
    users.create({
        name: name,
        username: username,
        email: email,
        password: password
    }).then(result => {
        res.status(201).json({
            data: result,
            message: 'Register succesfully'
        })
    }).catch(err => {
        console.log(err)
    })
}

exports.signInUser = (req, res) => {

}