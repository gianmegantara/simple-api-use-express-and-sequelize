const {
    check,
    validationResult
} = require('express-validator')

const validationRules = () => {
    return [check('title').not().isEmpty().withMessage('title harus diisi'),
        check('author').notEmpty().withMessage('author harus diisi'),
        check('published_date').notEmpty().withMessage('published date harus diisi'),
        check('pages').notEmpty().withMessage('pages harus diisi').isInt().withMessage('isi dengan angka'),
        check('language').notEmpty().withMessage('bahasa yang digunakan harus diisi'),
        check('publisher_id').notEmpty().withMessage('publisher harus diisi')
    ]
}

const validate = (req, res, next) => {
    const error = validationResult(req)
    if (error.isEmpty()) {
        return next()
    } else {
        return res.status(422).json({
            error: error.array()
        })
    }
}
module.exports = {
    validationRules,
    validate
}