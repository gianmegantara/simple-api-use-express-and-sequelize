const express = require('express')
const port = 8002
const dbConnection = require('./models/connection')
const route = require('./app/routes/route')

const app = express()
app.use(express.json())

app.use(route)

dbConnection.sync()
    .then(() => {
        app.listen(port, () => {
            console.log(`server was running on port ${port}`)
        })
    }).catch((err) => {
        console.log(err)
    })